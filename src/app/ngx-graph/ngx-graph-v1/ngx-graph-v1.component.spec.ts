import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NgxGraphV1Component } from './ngx-graph-v1.component';

describe('NgxGraphV1Component', () => {
  let component: NgxGraphV1Component;
  let fixture: ComponentFixture<NgxGraphV1Component>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [NgxGraphV1Component]
    });
    fixture = TestBed.createComponent(NgxGraphV1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
