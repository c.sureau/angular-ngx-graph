import { Component } from '@angular/core';
import { LINKS, NODES } from './../graph';
import { DagreSettings, Orientation } from '@swimlane/ngx-graph';

@Component({
  selector: 'app-ngx-graph-v1',
  templateUrl: './ngx-graph-v1.component.html',
  styleUrls: ['./ngx-graph-v1.component.scss'],
})
export class NgxGraphV1Component {
  links = LINKS;
  nodes = NODES;

  isReady = false;
  color = 'white';
  layoutSettings: DagreSettings = {
    orientation: Orientation.TOP_TO_BOTTOM,
    marginX: 10,
    marginY: 10,
    edgePadding: 250,
    rankPadding: 100,
    nodePadding: 10,
    multigraph: true,
  };

  view: [number, number] = [window.innerWidth, window.innerHeight];

  onNodeSelect($event: unknown) {
    console.log('detect the click on the node:', $event);
  }
}
