import { Component, Input, OnInit } from '@angular/core';
import { NodeExtended, Status } from '../graph-v2';

@Component({
  selector: 'app-node-template[node]',
  templateUrl: './node-template.component.html',
  styleUrls: ['./node-template.component.scss'],
})
export class NodeTemplateComponent implements OnInit {
  @Input() node!: NodeExtended;

  color = '';
  icon = '';

  ngOnInit(): void {
    this.initColorAndIcon();
  }

  private initColorAndIcon() {
    switch (this.node.status) {
      case Status.error:
        this.icon = 'error';
        this.color = 'red';
        break;
      case Status.success:
        this.icon = 'check_circle';
        this.color = 'green';
        break;
      case Status.waiting:
        this.icon = 'pending';
        this.color = 'lightblue';
        break;
      default:
        this.icon = 'rotate_right';
        this.color = 'lightblue';
        break;
    }
  }
}
