import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NodeTemplateComponent } from './node-template.component';

describe('NodeTemplateComponent', () => {
  let component: NodeTemplateComponent;
  let fixture: ComponentFixture<NodeTemplateComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [NodeTemplateComponent]
    });
    fixture = TestBed.createComponent(NodeTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
