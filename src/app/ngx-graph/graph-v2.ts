import { Edge, Node } from '@swimlane/ngx-graph';

export interface NodeExtended extends Node {
  status: Status;
}

export enum Status {
  running = 'running',
  success = 'success',
  error = 'error',
  waiting = 'waiting',
}

export const LINKS: Edge[] = [
  {
    id: 'link1',
    source: 'stepA',
    target: 'stepB',
  },
  {
    id: 'link2',
    source: 'stepA',
    target: 'stepC',
  },
];

export const NODES2: NodeExtended[] = [
  {
    id: 'stepA',
    label: 'Step A',
    status: Status.success,
  },
  {
    id: 'stepB',
    label: 'Step B',
    status: Status.running,
  },
  {
    id: 'stepC',
    label: 'Step C',
    status: Status.error,
  },
];
