import { Edge, Node } from '@swimlane/ngx-graph';

export const LINKS: Edge[] = [
  {
    id: 'link1',
    source: 'stepA',
    target: 'stepB',
  },
  {
    id: 'link2',
    source: 'stepA',
    target: 'stepC',
  },
];

export const NODES: Node[] = [
  {
    id: 'stepA',
    label: 'Step A',
  },
  {
    id: 'stepB',
    label: 'Step B',
  },
  {
    id: 'stepC',
    label: 'Step C',
  },
];
