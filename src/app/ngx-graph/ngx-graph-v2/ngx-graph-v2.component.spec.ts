import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NgxGraphV2Component } from './ngx-graph-v2.component';

describe('NgxGraphV2Component', () => {
  let component: NgxGraphV2Component;
  let fixture: ComponentFixture<NgxGraphV2Component>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [NgxGraphV2Component]
    });
    fixture = TestBed.createComponent(NgxGraphV2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
