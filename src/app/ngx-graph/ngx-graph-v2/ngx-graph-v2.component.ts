import { Component } from '@angular/core';
import { DagreSettings, Orientation } from '@swimlane/ngx-graph';
import { LINKS } from '../graph';
import { NODES2 } from '../graph-v2';

@Component({
  selector: 'app-ngx-graph-v2',
  templateUrl: './ngx-graph-v2.component.html',
  styleUrls: ['./ngx-graph-v2.component.scss'],
})
export class NgxGraphV2Component {
  links = LINKS;
  nodes = NODES2;

  isReady = false;
  color = 'white';
  layoutSettings: DagreSettings = {
    orientation: Orientation.TOP_TO_BOTTOM,
    marginX: 10,
    marginY: 10,
    edgePadding: 250,
    rankPadding: 100,
    nodePadding: 10,
    multigraph: true,
  };

  view: [number, number] = [window.innerWidth, window.innerHeight];

  onNodeSelect($event: unknown) {
    console.log('detect the click on the node:', $event);
  }
}
