import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { NgxGraphModule } from '@swimlane/ngx-graph';
import { NgxGraphV1Component } from './ngx-graph/ngx-graph-v1/ngx-graph-v1.component';
import { NgxGraphV2Component } from './ngx-graph/ngx-graph-v2/ngx-graph-v2.component';

const routes: Routes = [
  // { path: '', component: NgxGraphComponent },
  { path: 'ngx-graph-v2', component: NgxGraphV2Component },
  { path: 'ngx-graph-v1', component: NgxGraphV1Component },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
