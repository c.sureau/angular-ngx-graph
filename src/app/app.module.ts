import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationComponent } from './navigation/navigation.component';
import { NgxGraphModule } from '@swimlane/ngx-graph';
import { NodeTemplateComponent } from './ngx-graph/node-template/node-template.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxGraphV1Component } from './ngx-graph/ngx-graph-v1/ngx-graph-v1.component';
import { NgxGraphV2Component } from './ngx-graph/ngx-graph-v2/ngx-graph-v2.component';
@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    NodeTemplateComponent,
    NgxGraphV1Component,
    NgxGraphV2Component,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MatCardModule,
    MatButtonModule,
    NgxGraphModule,
    MatIconModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
